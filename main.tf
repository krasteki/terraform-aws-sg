variable "vpc_id" {
  description = "ID of the VPC where to create security group"
  type        = string
}

resource "aws_security_group" "aws_sg_module" {
  vpc_id      = var.vpc_id
  name = "aws_sg_module"
  ingress {
    from_port   = "8080"
    to_port     = "8080"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}